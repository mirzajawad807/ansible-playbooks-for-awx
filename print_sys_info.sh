#!/bin/sh


echo "Username...............: $(whoami)"
echo "Hostname...............: $(hostname)"
echo "Fully Qualified Name...: $(hostname -f)"
echo "Date...................: $(date)"
echo "IP Address.............: $(hostname -i)"
echo "Running Process........: $(ps aux | wc -l)"
