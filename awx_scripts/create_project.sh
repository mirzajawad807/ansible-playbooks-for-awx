#!/bin/sh
#
# Filename: create_project.sh
# Script will create or modify the Project on AWX server.
#
#

# User supplied arguments
AWX_PROJECT_NAME=${1}
SCM_URL=${2}

# Variables with default values
: ${SCM_CREDENTIAL:="gitlab-integration-ssh"}

if [[ ${#} -eq 0 ]]; then
  echo "ERROR: Please supply mandatory argument AWX Project Name."
  exit 1
fi

if [[ -z ${SCM_URL} ]]; then
  SCM_URL="git@${CI_SERVER_HOST}:${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git"
fi

# Print project names
if awx-cli project get -n ${AWX_PROJECT_NAME} &> /dev/null; then
  echo "Project already present on the AWX server."
  echo -e "AWX Project Name: ${AWX_PROJECT_NAME}\n"
  
  EXISITING_SCM_URL=$(awx-cli project get -n gitlab-integration-project --format json | jq -r .scm_url)
  if [[ ${SCM_URL} == ${EXISITING_SCM_URL} ]]; then
    echo "SCM URL matches with desired SCM repository."
    echo -e "SCM URL: ${SCM_URL}\n"
  else
    awx-cli project modify \
      -n ${AWX_PROJECT_NAME} \
      --scm-type git \
      --scm-url ${SCM_URL} \
      --scm-credential ${SCM_CREDENTIAL} \
      --scm-update-on-launch true
  fi
else
  echo "Creating AWX project ${AWX_PROJECT_NAME}..."
  awx-cli project create \
    -n ${AWX_PROJECT_NAME} \
    --organization 1 \
    --scm-type git \
    --scm-url ${SCM_URL} \
    --scm-credential ${SCM_CREDENTIAL} \
    --scm-update-on-launch true
fi
