#!/bin/bash
#
# Filename: deploy.sh
# Description: Deploy Ansible role to host of inventories via AWX
# 

# Variables with default values, user can override these variable by setting shell 
# environment variable
: ${AWX_PROJECT_NAME:="gitlab-integration-project"}
: ${AWX_JOB_TEMPLATE:="gitlab-integration-template"}
: ${AWX_INVENTORY:="gitlab-integration-inventory"}
: ${AWX_PLAYBOOK_NAME:="tasks/main.yaml"}

# Mandatory override variables
# TODO: Take variables as script argument or environment variable TBD
: ${AWX_SCM_ROLE_URL:=""}
: ${AWX_SCM_ROLE_CREDENTIAL:=""}
: ${AWX_INVENTORY_NAME:=""}
: ${AWX_INVENTORY_CREDENTIAL:=""}

if [[ -z ${AWX_SCM_ROLE_URL} ]]; then
  echo "Please provide the playbook repository URL."
  exit 1
fi

if [[ -z ${AWX_SCM_ROLE_CREDENTIAL} ]]; then
  echo "Please provide the playbook repository credential name or id."
  exit 1
fi

if [[ -z ${AWX_INVENTORY_NAME} ]]; then
  echo "Please provide the inventory name or id."
  exit 1
fi

if [[ -z ${AWX_INVENTORY_CREDENTIAL} ]]; then
  echo "Please provide the inventory credential."
  exit 1
fi

# TODO: Direct modification can be avoided if desired SCM url and credentials are already 
# configred by adding checks for SCM_URL and SCM_CREDENTIAL.
awx-cli project modify \
    -n ${AWX_PROJECT_NAME} \
    --scm-type git \
    --scm-url ${AWX_SCM_ROLE_URL} \
    --scm-credential ${AWX_SCM_ROLE_CREDENTIAL} \
    --scm-update-on-launch true

# Wait for SCM repo to update
sleep 10

awx-cli job_template modify \
  -n ${AWX_JOB_TEMPLATE} \
  -i ${AWX_INVENTORY_NAME} \
  --project ${AWX_PROJECT_NAME} \
  --credential ${AWX_INVENTORY_CREDENTIAL} \
  --playbook ${AWX_PLAYBOOK_NAME} \
  --verbosity verbose \
  --become-enabled true

# Launch a job and wait for its completions.
awx-cli job wait $(awx-cli job launch -J gitlab-integration-template --format json | jq .id)

