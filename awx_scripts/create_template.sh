#!/bin/sh
#
# Filename: create_template.sh
# Script will create or modify the Template on AWX server.
#
#

# User supplied arguments
AWX_TEMPLATE_NAME=${1}
PLYABOOK_NAME=${2}

# Variables with default values
: ${INVENTORY_NAME:="gitlab-integration-inventory"}
: ${INVENTORY_CREDENTIAL:="gitlab-inventory-credential"}



if [[ ${#} -eq 0 ]]; then
  echo "ERROR: Please supply mandatory argument AWX Project Name."
  exit 1
fi

if awx-cli job_template get -n ${AWX_TEMPLATE_NAME} --format json | jq -r .name &> /dev/null; then

  EXISTING_PLAYBOOK=$(awx-cli job_template get -n ${AWX_TEMPLATE_NAME} --format json | jq -r .playbook 2> /dev/null;)
  
  if [[ -n ${PLYABOOK_NAME} ]] && [[ ${EXISTING_PLAYBOOK} == ${PLYABOOK_NAME} ]]; then

    awx-cli job_template modify \
      -n ${AWX_TEMPLATE_NAME} \
      --project ${AWX_PROJECT_NAME} \
      --playbook ${PLYABOOK_NAME} \
      --credential ${INVENTORY_CREDENTIAL} \
      --verbosity verbose \
      --become-enabled true
  fi
else
  awx-cli job_template create \
    -n ${AWX_TEMPLATE_NAME} \
    --job-type run \
    -i ${INVENTORY_NAME} \
    --project ${AWX_PROJECT_NAME} \
    --playbook ${PLYABOOK_NAME} \
    --credential ${INVENTORY_CREDENTIAL} \
    --verbosity verbose \
    --become-enabled true
fi
